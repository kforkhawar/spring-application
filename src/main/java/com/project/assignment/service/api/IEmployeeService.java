package com.project.assignment.service.api;

import com.project.assignment.persistence.entity.Department;
import com.project.assignment.persistence.entity.Employee;

import java.util.List;
import java.util.Optional;

public interface IEmployeeService {
    List<Employee> getAllEmployees();
    Employee saveEmployee(Employee e);
    List<Department> getAllDepartments();

}
