package com.project.assignment.service.impl;

import com.project.assignment.persistence.DaoFactory;
import com.project.assignment.persistence.entity.Department;
import com.project.assignment.persistence.entity.Employee;
import com.project.assignment.service.api.IEmployeeService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Getter @Setter
public class EmployeeServiceImpl implements IEmployeeService, InitializingBean {

    @Autowired
    DaoFactory daoFactory;

    private final List<Employee> employeeList = new ArrayList<>();
    private final List<Department> departmentList = new ArrayList<>();



    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(daoFactory,"daoFactory is Required");
    }

    @Override
    public List<Employee> getAllEmployees() {
        List<Employee> employeeList = daoFactory.getEmployeeRepo().findAll();
        return employeeList==null ? new ArrayList<>() : employeeList ;
    }

    @Override
    public Employee saveEmployee(Employee e) {

        if(e.getDepartment()!=null){
            Optional<Department> departmentById = daoFactory.getDepartmentRepo().findById(e.getDepartment().getDepartmentId());
            if(departmentById.isPresent())
                e.setDepartment(departmentById.get());
            else
                e.setDepartment(null);
        }
        return daoFactory.getEmployeeRepo().save(e);
    }

    @Override
    public List<Department> getAllDepartments() {
        if(departmentList.size() == 0){
            // 1. Add some departments
            departmentList.add(getDepartment("Development",null));
            departmentList.add(getDepartment("SQA",null));
            departmentList.add(getDepartment("Research",null));
            daoFactory.getDepartmentRepo().saveAll(departmentList);
        }
        return daoFactory.getDepartmentRepo().findAll();
    }

    // #####################################
    //          Private Methods
    // #####################################

    private Department getDepartment(String name, Employee manger){
        Department d = new Department();
        d.setDepartmentName(name);
        d.setManager(manger);
        return d;
    }

    private Employee getEmployee(String f, String l, String e, String p, Date h, int s, Department d, Employee m){
        Employee employee = new Employee();
        employee.setDepartment(d);
        employee.setEmail(e);
        employee.setFirstName(f);
        employee.setLastName(l);
        employee.setPhoneNumber(p);
        employee.setHireDate(h);
        employee.setSalary(s);
        employee.setManager(m);
        return employee;
    }
}
