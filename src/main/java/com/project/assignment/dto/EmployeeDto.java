package com.project.assignment.dto;

import lombok.Getter;
import lombok.Setter;


import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter @Setter
public class EmployeeDto {

    private Integer _id;
    @NotNull(message = "firstName cannot be null")
    private String firstName;

    @NotNull(message = "lastName cannot be null")
    private String lastName;
    private String email;
    private Date hireDate;
    private String phoneNumber;
    private int salary;
    private EmployeeDto manager;
    private DepartmentDto department;

}
