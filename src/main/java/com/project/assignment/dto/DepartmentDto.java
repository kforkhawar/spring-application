package com.project.assignment.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DepartmentDto {

    private Integer _id;
    private String name;
    private EmployeeDto manager;
}
