package com.project.assignment.persistence.repo;

import com.project.assignment.persistence.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IEmployeeRepo extends JpaRepository<Employee,Integer> {

}
