package com.project.assignment.persistence.repo;

import com.project.assignment.persistence.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDepartmentRepo  extends JpaRepository<Department,Integer> {
}
