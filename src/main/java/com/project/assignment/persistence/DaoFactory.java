package com.project.assignment.persistence;

import com.project.assignment.persistence.repo.IDepartmentRepo;
import com.project.assignment.persistence.repo.IEmployeeRepo;
import com.project.assignment.service.api.IEmployeeService;
import lombok.Getter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
@Getter
public class DaoFactory implements InitializingBean {
    @Autowired
    IEmployeeRepo employeeRepo;

    @Autowired
    IDepartmentRepo departmentRepo;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(employeeRepo,"employeeRepo is Required");
        Assert.notNull(departmentRepo,"departmentRepo is Required");
    }
}
