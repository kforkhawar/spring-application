package com.project.assignment.controller;

import com.project.assignment.dto.DepartmentDto;
import com.project.assignment.dto.EmployeeDto;
import com.project.assignment.persistence.entity.Department;
import com.project.assignment.persistence.entity.Employee;
import com.project.assignment.service.api.IEmployeeService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "https://shrouded-chamber-98003.herokuapp.com")
@RequestMapping(value="api")
class EmployeeController implements InitializingBean {


    @Autowired
    IEmployeeService employeeService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(employeeService,"employeeService is Required");
    }

    @GetMapping(value = "employee")
    public List<EmployeeDto> getEmployee(){

        List<Employee> customers = employeeService.getAllEmployees();
        List<EmployeeDto> employeeDtoList = customers.stream().map(employee -> {
            EmployeeDto dto = new EmployeeDto();
            dto.set_id(employee.getEmployeeId());
            dto.setFirstName(employee.getFirstName());
            dto.setLastName(employee.getLastName());
            dto.setEmail(employee.getEmail());
            dto.setHireDate(employee.getHireDate());
            dto.setPhoneNumber(employee.getPhoneNumber());
            dto.setSalary(employee.getSalary());
            if (employee.getManager() != null) {
                EmployeeDto managerDto = new EmployeeDto();
                managerDto.set_id(employee.getManager().getEmployeeId());
                managerDto.setFirstName(employee.getManager().getFirstName());
                dto.setManager(managerDto);
            }

            if (employee.getDepartment() != null) {
                DepartmentDto departmentDto = new DepartmentDto();
                departmentDto.set_id(employee.getDepartment().getDepartmentId());
                departmentDto.setName(employee.getDepartment().getDepartmentName());
                dto.setDepartment(departmentDto);
            }

            return dto;
        }).collect(Collectors.toList());
        return employeeDtoList;
    }

    @PostMapping(value = "employee")
    public Employee addEmployee(@Valid @RequestBody EmployeeDto employeeDto){

        Employee e = getEmployee(employeeDto);
        if(employeeDto.getDepartment()!=null){
            Department department = new Department();
            department.setDepartmentId(employeeDto.getDepartment().get_id());
            department.setDepartmentName(employeeDto.getDepartment().getName());
            e.setDepartment(department);
            e.setManager(null);
        }
        return employeeService.saveEmployee(e);
    }


    @GetMapping(value = "departments")
    public List<DepartmentDto> getAllDepartments(){
        List<Department> allDepartments = employeeService.getAllDepartments();
        List<DepartmentDto> departmentDtoList = allDepartments.stream().map(x -> {
            DepartmentDto dto = new DepartmentDto();
            dto.set_id(x.getDepartmentId());
            dto.setName(x.getDepartmentName());
            if(x.getManager()!=null){
                EmployeeDto employeeDto = new EmployeeDto();
                employeeDto.set_id(x.getManager().getEmployeeId());
                employeeDto.setFirstName(x.getManager().getFirstName());
                dto.setManager(employeeDto);
            }
            return dto;
        }).collect(Collectors.toList());

        return departmentDtoList;
    }

    // Private
    Employee getEmployee(EmployeeDto employeeDto) {
        Employee e = new Employee();
        e.setFirstName(employeeDto.getFirstName());
        e.setLastName(employeeDto.getLastName());
        e.setEmail(employeeDto.getEmail());
        if (employeeDto.getHireDate() == null){
            e.setHireDate(new Date());
        }else{
            e.setHireDate(employeeDto.getHireDate());
        }
        e.setPhoneNumber(employeeDto.getPhoneNumber());
        e.setSalary(employeeDto.getSalary());
        return e;
    }

}